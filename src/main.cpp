#include <mylogger.hpp>

int main () {


     my::Logger L ("my::Logger", (my::Logger::TO_FILE   |
				  my::Logger::TO_STDOUT |
				  my::Logger::TO_SYSLOG ),
		   "test");

     L.Message (">TEST\nmessage!<");
     L.Error   (">TEST\nerror!<");
     L.Warning (">TEST\nwarning!<");
     L.Debug   (">TEST\ndebug!<");


     return 0;
}
