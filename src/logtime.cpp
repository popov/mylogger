/* Copyright (c) 2012, Boris Popov <popov.b@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <logtime.hpp>
#include <iomanip>
#include <sstream>
#include <time.h>

// ------------------------------------------------ //
my::LogTime::LogTime () {

     m_TimeVal.tv_sec = 0;
     m_TimeVal.tv_usec = 0;
     gettimeofday (&m_TimeVal, NULL);
     return;
}

my::LogTime::~LogTime () {

     return;
}

my::TimeString my::LogTime::GetAsString () {

     struct tm mtm;

     gmtime_r ( (&m_TimeVal.tv_sec), (&mtm) );
     std::stringstream temp;

     int day = mtm.tm_mday;
     temp << std::setfill ('0') << std::setw (2) << day;
     temp << "-";

     int mon  = (mtm.tm_mon) + 1;
     temp << std::setfill ('0') << std::setw (2) << mon;
     temp << "-";

     int year = (mtm.tm_year) + 1900;
     temp << std::setfill ('0') << std::setw (4) << year;
     temp << " ";

     int hour = mtm.tm_hour;
     temp << std::setfill ('0') << std::setw (2) << hour;
     temp << ":";

     int min = mtm.tm_min;
     temp << std::setfill ('0') << std::setw (2) << min;
     temp << ":";

     int sec = mtm.tm_sec;
     temp << std::setfill ('0') << std::setw (2) << sec;
     temp << ".";

     int msec = m_TimeVal.tv_usec;
     temp << std::setfill ('0') << std::setw (6) << msec;

     return temp.str ();
}
// ------------------------------------------------ //
