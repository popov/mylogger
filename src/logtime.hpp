/* Copyright (c) 2012, Boris Popov <popov.b@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <sys/time.h>

#ifndef __logtime_hpp__
#define __logtime_hpp__

namespace my {

// ------------------------------------------------ //
     typedef std::string String;
     typedef String TimeString;
// ------------------------------------------------ //

// ------------------------------------------------ //
     class LogTime {

     public:
	  LogTime ();
	  ~LogTime ();

	  TimeString GetAsString ();

     private:
	  struct timeval m_TimeVal;
     };
// ------------------------------------------------ //
}
#endif // __logtime_hpp__
