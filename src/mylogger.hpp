/* Copyright (c) 2012, Boris Popov <popov.b@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>

#ifndef __mylogger_hpp__
#define __mylogger_hpp__

namespace my {

// ------------------------------------------------ //
     typedef std::string String;
     typedef String FileName;
     typedef String Ident;
// ------------------------------------------------ //

// ------------------------------------------------ //
     class Logger {

     public:
	  enum PRIO { MESSAGE, ERROR, WARNING, DEBUG };

	  enum Destination {

	       TO_STDOUT = (1 << 0),
	       TO_STDERR = (1 << 1),
	       TO_FILE   = (1 << 2),
	       TO_SYSLOG = (1 << 3),
	  };

     public:
	  Logger (const Ident, const int, const FileName);
	  ~Logger ();

	  void Message (const String);
	  void Error   (const String);
	  void Warning (const String);
	  void Debug   (const String);

     private:
	  const Ident m_Ident;

	  Logger (const Logger&);
	  Logger& operator= (const Logger&);

	  const int m_Destination;
	  int fd;

	  String m_GetPrefix () const;
	  void m_Out (const PRIO, const String) const;

	  String m_GetPid () const;

	  int m_GetPrioSys (const PRIO) const;
     };
// ------------------------------------------------ //
}
#endif // __mylogger_hpp__
