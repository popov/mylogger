/* Copyright (c) 2012, Boris Popov <popov.b@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <mylogger.hpp>
#include <logtime.hpp>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include <syslog.h>
#include <sstream>

// ------------------------------------------------ //
my::Logger::Logger (const Ident id, const int d, const FileName f):

     m_Ident (id),
     m_Destination (d),
     fd (0)
{
     fd = open (f.c_str (), O_APPEND | O_WRONLY);

     if (m_Destination & TO_SYSLOG) {

	  if (m_Ident.size () != 0) {

	       openlog (m_Ident.c_str (), 0, 0);

	  } else {

	       openlog (NULL, 0, 0);
	  }
     }
     return;
}

my::Logger::~Logger () {

     if (fd > 0)  close (fd);
     if (m_Destination & TO_SYSLOG) {

	  closelog ();
     }
     return;
}

void my::Logger::Message (const String str) {

     String message ( m_GetPrefix () );
     message += " MESSAGE:\r\n";
     message += str;
     message += "\r\n***\r\n";
     m_Out (MESSAGE, message);
     return;
}

my::String my::Logger::m_GetPrefix () const {

     LogTime LT;
     return (LT.GetAsString () + " pid=" + m_GetPid ());
}

void my::Logger::m_Out (const PRIO pr, const String str) const {

     if (m_Destination & TO_STDOUT) {

	  write (STDOUT_FILENO, str.c_str (), str.size ());
	  fsync (STDOUT_FILENO);
     }

     if (m_Destination & TO_STDERR) {

	  write (STDERR_FILENO, str.c_str (), str.size ());
     }

     if (m_Destination & TO_FILE) {

	  if (fd > 0) {

	       write (fd, str.c_str (), str.size ());
	       fsync (fd);
	  }
     }

     if (m_Destination & TO_SYSLOG) {

	  int prio = m_GetPrioSys (pr);
	  syslog (prio, "%s", str.c_str ());
     }
     return;
}

void my::Logger::Error (const String str) {

     String message ( m_GetPrefix () );
     message += " ERROR:\r\n";
     message += str;
     message += "\r\n***\r\n";
     m_Out (ERROR, message);
}

void my::Logger::Warning (const String str) {

     String message ( m_GetPrefix () );
     message += " WARNING:\r\n";
     message += str;
     message += "\r\n***\r\n";
     m_Out (WARNING, message);
}

void my::Logger::Debug (const String str) {

     String message ( m_GetPrefix () );
     message += " DEBUG:\r\n";
     message += str;
     message += "\r\n***\r\n";
     m_Out (DEBUG, message);
}

my::String my::Logger::m_GetPid () const {

     pid_t pid = getpid ();
     std::stringstream ss;
     ss << pid;
     return ss.str ();
}

int my::Logger::m_GetPrioSys (const PRIO pr) const {

     switch (pr) {

     case MESSAGE:
	  return LOG_INFO;

     case ERROR:
	  return LOG_ERR;

     case WARNING:
	  return LOG_WARNING;

     case DEBUG:
	  return LOG_DEBUG;
     }
     return 0;
}
// ------------------------------------------------ //
